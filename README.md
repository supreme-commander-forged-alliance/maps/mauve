
[![License: CC BY-SA-NC 4.0](https://img.shields.io/badge/License-CC%20BY--SA--NC%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa-nc/4.0/)

# Mauve

![](images/overview.png)
_A peaceful valley transformed into a wasteland by the rivalry between the UEF and the Cybran Nation_

### Statistics on the map

![](images/preview-1.png)

The map is designed for a casual 2v2 setup or a competitive 1v1 setup. Make sure to use the first two slots when you play it as a 1v1. The back slot is the casual slot due to limited strategic oppertunities.

The center is a tricky place - it holds no direct strategic value but all the resources surrounding the center can be hit by artillery from down below. You need to secure it in order to protect your investments. 

There is quite an amount of reclaim on the map:
 - In the form of trees, providing both mass and power.
 - In the form of rocks, providing mass.
 - In the form of civilians / wreckages, providing mass.

The total prop reclaim value is estimated to be:
 - 19K mass
 - 80K energy

![](images/preview-2.png)

### Technical aim

![](images/shore.png)

One major aim was to create an interesting beachline. This was accomplished by generating a mask that represents the shore, terracing that mask and then using various nodes to create a dune like pattern. The pattern is slightly distorted to make it appear more natural.

![](images/cliffs.png)

Another aim was to understand the erosion that is primarily used for creating the cliffs. There are various options available, one more useful than the other. Particularly the global intensifier causes more issues than one would image.

And last but not least the addition of heightlines in the terrain. These are well-visible in the overview. They are only visible when you are zoomed out - allowing you to assess the roughness of the terrain from a birds-eye view. 

### Installation guide

There are two maps present in this repository. Make sure to use the non-core version. The core version is used as part of the development cycle - it is not playable.

Move the non-core map to the following folder:
 - C:\Users\%USER%\Documents\My Games\Gas Powered Games\Supreme Commander Forged Alliance\maps

Where %USER% is your windows username. If the maps folder does not exist you can create one. The map is also available in the FAF and LOUD map vaults.

### Credits

Design is made by Sebastian Brinkmann (Leto_II) during a community collaboration on map layouts:
 - https://forum.faforever.com/topic/1214/a-community-effort-on-a-map-layout

## License

The stratum layers (/env/layers) are from www.textures.com. I am obligated to add this text:
_One or more textures bundled with this project have been created with images from Textures.com. These images may not be redistributed by default. Please visit www.textures.com for more information._

All other assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
