version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Mauve",
    description = "A map based on the design of Leto as part of: https://forum.faforever.com/topic/1214/a-community-effort-on-a-map-layout/",
    preview = '',
    map_version = 1,
    type = 'skirmish',
    starts = true,
    size = {512, 512},
    reclaim = {0, 0},
    map = '/maps/mauve_core/Mauve.scmap',
    save = '/maps/mauve_core/Mauve_save.lua',
    script = '/maps/mauve_core/Mauve_script.lua',
    norushradius = 40,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
