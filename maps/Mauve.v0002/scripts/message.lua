
do

    ForkThread(function ()

        WaitSeconds(16.0);

        local text = 'center \r\n enter \r\n row3';
        local size = 20;
        local fade = 9;

        -- what other options are there?
        local alignment = 'lefttop';

        -- what format should the color be in?
        local color = 'FFFFFF';

        -- alignment
        local offset = "          "
        for k = 1, 7 do 
            PrintText("", size + 10, color, fade, alignment);
        end

        -- print title
        PrintText(offset .. "Mauve", size + 10, color, fade, alignment);
        PrintText(offset .. offset .. "A Collab with Leto_II", size - 2, color, fade, alignment);
        PrintText("", size + 10, color, fade, alignment);
        WaitSeconds(2.0)

        -- print graphics
        PrintText(offset .. " Recommended graphics ", size + 5, color, fade, alignment);
        PrintText(offset .. "     Fidelity:              high ", size, color, fade, alignment);
        PrintText(offset .. "     Texture detail:    high ", size, color, fade, alignment);
        PrintText(offset .. "     Bloom Render:   on ", size, color, fade, alignment);

    end);

end
